var fs = require('fs');
var path = require('path');

var cache = { 0: 0, 1: 1, 2: 1 };
function fibonacci(n) {
  if (n in cache) {
    var answer = cache[n];
  } else {
    var answer = fibonacci(n-1) + fibonacci(n-2);
    cache[n] = answer;
  }
  return answer;
}

if (require.main === module) {
  var fileName = process.argv[2];
  var instanceId = process.argv[3];
  var launchIndex = process.argv[4];
  console.log('-- write-fibonacci-file.js -- launchIndex:', launchIndex, '-- instanceId:', instanceId);

  var fibs = [];
  for (var n=1; n<30; n++) {
    fibs.push({ n: n, fib: fibonacci(n) });
  }

  var lines = fibs.map(({ n, fib }) => `${n}: ${fib}`);
  writeFile(lines, fileName);
}

function writeFile(lines, fileName) {
  var filePath = path.join(__dirname, fileName);
  var file = fs.createWriteStream(filePath, { flags: 'w' });
  file.on('error', err => console.error('Error writing to file! -- ' + err));
  lines.forEach(line => file.write(line + '\n'));
  file.end();
  console.log('writeFile was successful -- output file:', filePath);
}

